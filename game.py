def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()


def game_over(board, winner):
    print_board(board)
    print("GAME OVER")
    print(winner, "has won")
    exit()


def is_row_winner(board, row_number):
    result = False
    row_win = 0
    if board[0] == board[1] and board[1] == board[2]: 
        row_win = 1
    elif board[3] == board[4] and board[4] == board[5]:
        row_win = 2
    elif board[6] == board[7] and board[7] == board[8]:
        row_win = 3
    
    if (row_number == row_win):
        result = True
    return result


def is_column_winner(board, column_number):
    result = False
    column_win = 0
    if board[0] == board[3] and board[3] == board[6]: 
        column_win = 1
    elif board[1] == board[4] and board[4] == board[7]:
        column_win = 2
    elif board[2] == board[5] and board[5] == board[8]:
        column_win = 3
    
    if (column_number == column_win):
        result = True
    return result


def is_diagonal_winner(board, diagonal_number):
    result = False
    diagonal_win = 0
    if board[0] == board[4] and board[4] == board[8]:
        diagonal_win = 1
    elif board[2] == board[4] and board[4] == board[6]:
        diagonal_win = 2
    
    if (diagonal_number == diagonal_win):
        result = True
    return result

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 1):
        game_over(board, board[0])
    elif is_row_winner(board, 2):
        game_over(board, board[3])
    elif is_row_winner(board, 3):        
        game_over(board, board[6])
    elif is_column_winner(board, 1):
        game_over(board, board[3])
    elif is_column_winner(board, 2):
        game_over(board, board[1])
    elif is_column_winner(board, 3):
        game_over(board, board[2])
    elif is_diagonal_winner(board, 1):
        game_over(board, board[4])
    elif is_diagonal_winner(board, 2):
        game_over(board, board[2])

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
